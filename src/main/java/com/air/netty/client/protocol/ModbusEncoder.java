package com.air.netty.client.protocol;

import com.air.common.util.StrUtils;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @Description
 * @Author semstouch
 * @Date 2016/12/9
 **/
public class ModbusEncoder extends MessageToByteEncoder<Modbus> {
    protected static final Logger logger = LoggerFactory.getLogger(ModbusEncoder.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, Modbus msg,
                          ByteBuf out) throws Exception {
        if (msg == null) {
            throw new Exception("The encode message is null");
        }

        StringBuffer data = new StringBuffer();
        data.append("68");
        data.append(msg.getUID());
        data.append("68");
        data.append(msg.getCODE());
        data.append(StrUtils.getHexString2(msg.getDATA().length() / 2));
        data.append(msg.getDATA());
        byte[]  dataByte= StrUtils.hexStringToBytes(data.toString());
        byte crc = StrUtils.getCRC(dataByte);
        String crcStr = StrUtils.byteToHexString(crc);
        data.append(crcStr);
        data.append("16");
        logger.debug("发送指令："+data.toString());
        out.writeBytes(StrUtils.hexStringToBytes(data.toString()));
    }
}
