package com.air.service;

import com.air.pojo.AirDevice;
import com.air.pojo.AirUser;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2017/1/19.
 */
@Service("airDeviceService")
public interface AirDeviceService {
     List queryDevice(String openid);
     List queryDeviceOpenid(String uid);
     AirDevice selectByUid(String device_uid,String openid);
     boolean addDevice(AirDevice airDevice,AirUser airUser);
     boolean updateDevice(AirDevice airDevice);
     boolean delDevice(AirDevice airDevice);
    boolean restFilter(AirDevice airDevice);


}
