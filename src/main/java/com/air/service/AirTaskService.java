package com.air.service;

import com.air.pojo.AirTask;
import org.springframework.stereotype.Service;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/7/24
 **/
@Service
public interface AirTaskService {

    int deleteByPrimaryKey(Integer id);

    int insert(AirTask record);

    int insertSelective(AirTask record);

    AirTask selectByPrimaryKey(Integer id);

    AirTask selectByUid(String uid);

    int updateByPrimaryKeySelective(AirTask record);

    int updateByPrimaryKey(AirTask record);
}
