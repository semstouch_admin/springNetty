package com.air.service.impl;

import com.air.mapper.AirTaskMapper;
import com.air.pojo.AirTask;
import com.air.service.AirTaskService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/7/24
 **/
@Service
public class AirTaskServiceImpl implements AirTaskService{
    @Resource
    private AirTaskMapper airTaskMapper;
    @Override
    public int deleteByPrimaryKey(Integer id) {
        return airTaskMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int insert(AirTask record) {
        return airTaskMapper.insert(record);
    }

    @Override
    public int insertSelective(AirTask record) {
        return airTaskMapper.insertSelective(record);
    }

    @Override
    public AirTask selectByPrimaryKey(Integer id) {
        return airTaskMapper.selectByPrimaryKey(id);
    }

    @Override
    public AirTask selectByUid(String uid) {
        return airTaskMapper.selectByUid(uid);
    }

    @Override
    public int updateByPrimaryKeySelective(AirTask record) {
        return airTaskMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public int updateByPrimaryKey(AirTask record) {
        return airTaskMapper.updateByPrimaryKey(record);
    }
}
