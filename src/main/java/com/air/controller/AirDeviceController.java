package com.air.controller;

import com.air.netty.client.protocol.Modbus;
import com.air.pojo.AirDevice;
import com.air.pojo.AirTask;
import com.air.pojo.AirUser;
import com.air.service.AirDeviceService;
import com.air.common.util.JSONResult;
import com.air.service.AirTaskService;
import io.netty.channel.Channel;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2017/1/19.
 */
@Controller
@RequestMapping("/device")
public class AirDeviceController {
    @Resource
    private AirDeviceService airDeviceService;
    @Resource
    private AirTaskService airTaskService;

    /**
     * 绑定设备信息
     * @param airDevice 设备信息
     * @param request  request
     * @return JSONResult
     */
    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public  JSONResult add(@ModelAttribute("airDevice") AirDevice airDevice,HttpServletRequest request){
        JSONResult result=new JSONResult();
        AirUser airUser = (AirUser)request.getSession().getAttribute("airUser");
        if(airUser==null){
            result.setMessage("onLogin");
            return result;
        }
        //需要在此处查询是否归属
        AirDevice airDeviceOld = airDeviceService.selectByUid(airDevice.getDevice_uid(),airUser.getOpenid());
        if(airDeviceOld!=null){
            result.setMessage("isIn");
            return result;
        }
        if(airDeviceService.addDevice(airDevice,airUser)){
            result.setMessage("success");
        }else {
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 绑定设备任务
     * @param airTask 设备任务
     * @param request  request
     * @return JSONResult
     */
    @RequestMapping(value = "/task",method = RequestMethod.POST)
    @ResponseBody
    public  JSONResult addTask(@ModelAttribute("airDevice") AirTask airTask,HttpServletRequest request){
        JSONResult result=new JSONResult();
        AirUser airUser = (AirUser)request.getSession().getAttribute("airUser");
        if(airUser==null){
            result.setMessage("onLogin");
            return result;
        }
        //需要在此处查询是否归属
        AirDevice airDeviceOld = airDeviceService.selectByUid(airTask.getUid(), airUser.getOpenid());
        if(airDeviceOld==null){
            result.setMessage("isNo");
            return result;
        }
        ServletContext servletContext=request.getSession().getServletContext();
        if(airTaskService.insertSelective(airTask)==1){
            Map clientSocket=(Map) servletContext.getAttribute("clientMap");
            Channel incoming = (Channel)clientSocket.get(airTask.getUid());
            Modbus modbus = new Modbus();
            if(incoming!=null) {
                modbus.setUID(airTask.getUid());
                modbus.setCODE("82");
                modbus.setDATA(airTask.getTaskTime());
                incoming.writeAndFlush(modbus);
            }
            result.setMessage("success");
        }else {
            result.setMessage("error");
        }
        return result;
    }

    /**
     *根据uid删除设备信息
     * @param request request
     * @param uid uid
     * @return JSONResult
     */
    @RequestMapping(value = "/del",method = RequestMethod.POST)
    @ResponseBody
    public  JSONResult del(HttpServletRequest request,String uid){
        JSONResult result=new JSONResult();
        AirUser airUser = (AirUser)request.getSession().getAttribute("airUser");
        if(airUser==null){
            return result;
        }
        //需要在此处查询是否归属
        AirDevice airDevice = airDeviceService.selectByUid(uid,airUser.getOpenid());
        if(airDevice==null){
            result.setMessage("noIn");
            return result;
        }
        if(airDeviceService.delDevice(airDevice)){
            result.setMessage("success");
        }else {
            result.setMessage("error");
        }
        return result;
    }

    /**
     *根据uid重置滤网
     * @param request request
     * @param uid uid
     * @return JSONResult
     */
    @RequestMapping(value = "/restFilter",method = RequestMethod.POST)
    @ResponseBody
    public  JSONResult restFilter(HttpServletRequest request,String uid){
        JSONResult result=new JSONResult();
        AirUser airUser = (AirUser)request.getSession().getAttribute("airUser");
        //需要在此处查询是否归属
        AirDevice airDevice = airDeviceService.selectByUid(uid, airUser.getOpenid());
        if(airDevice==null){
            result.setMessage("noIn");
            return result;
        }
        if(airDeviceService.restFilter(airDevice)){
            result.setMessage("success");
        }else {
            result.setMessage("error");
        }
        return result;
    }

    /**
     * 根据openId查找全部的设备信息
     * @param request request
     * @return JSONResult
     */
    @RequestMapping(value = "/find", method = RequestMethod.GET)
    @ResponseBody
    public JSONResult find(HttpServletRequest request){
        JSONResult result=new JSONResult();
        AirUser airUser = (AirUser)request.getSession().getAttribute("airUser");

        if(airUser==null){
            return result;
        }
        List<AirDevice> list= airDeviceService.queryDevice(airUser.getOpenid());
        Map airDeviceMap = new HashMap();
        airDeviceMap.put("list",list);
        result.setData(airDeviceMap);
        return result;
    }

}
