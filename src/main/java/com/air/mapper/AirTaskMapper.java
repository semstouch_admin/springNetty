package com.air.mapper;

import com.air.pojo.AirTask;

public interface AirTaskMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(AirTask record);

    int insertSelective(AirTask record);

    AirTask selectByPrimaryKey(Integer id);

    AirTask selectByUid(String uid);

    int updateByPrimaryKeySelective(AirTask record);

    int updateByPrimaryKey(AirTask record);
}