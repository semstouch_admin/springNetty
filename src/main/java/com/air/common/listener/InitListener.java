package com.air.common.listener;

import com.air.common.constant.WxUrlType;
import com.air.common.entity.AccessTokenEntity;
import com.air.pojo.AirWxInfo;
import com.air.service.AirWxInfoService;
import com.air.service.NettyService;
import com.air.service.WebSocketService;
import com.air.common.util.WxUtil;
import io.netty.channel.Channel;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;

import javax.annotation.Resource;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 内容管理平台服务启动监听器
 * 查询字典数据存入缓存
 * Created by Administrator on 2016/9/2.
 */
@Component
public class InitListener implements InitializingBean,ServletContextAware{
    protected static final org.slf4j.Logger logger = LoggerFactory.getLogger(InitListener.class);

    @Resource
    private NettyService nettyService;
   @Resource
    private WebSocketService webSocketService;
    @Resource
    private AirWxInfoService airWxInfoService;
    public void  afterPropertiesSet() throws Exception{}
    public void setServletContext(ServletContext servletContext){
        AirWxInfo airWxInfo = airWxInfoService.queryWxInfo();
        if(airWxInfo==null){
            logger.error("数据库未存储微信公众号配置信息！");
        }
        servletContext.setAttribute("wxinfo",airWxInfo);

        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config/conf.properties");
        Properties p = new Properties();
        try{
            p.load(inputStream);
        } catch (IOException e1){
            e1.printStackTrace();
        }
        servletContext.setAttribute("template_id",p.getProperty("wx.template_id"));


        Map<String,Channel> clientMap=new HashMap();
        Map<String,Channel> websocketMap=new HashMap();
        Map<String,String> ipUIDMap=new HashMap();
        Map<String,String> wsUIDMap = new HashMap();
        servletContext.setAttribute("ipUIDMap",ipUIDMap);
        servletContext.setAttribute("wsUIDMap",wsUIDMap);
        servletContext.setAttribute("clientMap",clientMap);
        servletContext.setAttribute("websocketMap",websocketMap);

        this.initAccessToken(servletContext);
        this.initNettyServer(servletContext);

    }

    /**
     * 启动长连接监听进程服务
     * @param servletContext
     */
    public void initNettyServer(ServletContext servletContext){
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config/netty.properties");
        Properties p = new Properties();
        try{
            p.load(inputStream);
        } catch (IOException e1){
            e1.printStackTrace();
        }
        Thread client = new Thread(new Runnable(){
            public void run(){
                nettyService.start(Integer.parseInt(p.getProperty("client.port")),servletContext);
            }});
        client.start();

        Thread websocket = new Thread(new Runnable(){
            public void run(){
                webSocketService.start(Integer.parseInt(p.getProperty("websocket.port")),servletContext);
            }});
        websocket.start();
    }

    /**
     * 获取微信 accessToken
     * @param servletContext
     */
    public void initAccessToken(ServletContext servletContext){
        AirWxInfo airWxInfo = (AirWxInfo)servletContext.getAttribute("wxinfo");
        Map<String,String> params = new HashMap<String,String>();
        params.put("grant_type","client_credential");
        params.put("appid",airWxInfo.getAppid());
        params.put("secret",airWxInfo.getSecret());
        AccessTokenEntity accessTokenEntity = WxUtil.sendRequest(WxUrlType.tokenUrl, HttpMethod.GET, params, null, AccessTokenEntity.class);
        if(accessTokenEntity==null){
            logger.error("首次获取微信 accessToken失败！");
        }

        servletContext.setAttribute("accessToken",accessTokenEntity);

        logger.info("首次获取accessToken为"+(accessTokenEntity!=null?accessTokenEntity.getAccess_token():""));
    }



}
