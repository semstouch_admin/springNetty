package com.air.common.util;

import com.air.common.constant.WxUrlType;
import com.air.common.entity.AccessTokenEntity;
import com.air.common.entity.StautsMsgTemplateEntity;
import com.air.common.entity.WxRespCodeEntity;
import org.apache.http.entity.StringEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;

import javax.servlet.ServletContext;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @Description
 * @Author semstouch
 * @Date 2017/7/16
 **/
public class WxMsgUtil {
    protected static final Logger logger = LoggerFactory.getLogger(WxMsgUtil.class);

    public static boolean sendLoginMsg(String UID,ServletContext servletContext,String openid){
        AccessTokenEntity accessToken = (AccessTokenEntity)servletContext.getAttribute("accessToken");
        StautsMsgTemplateEntity msgTemplateEntity= new StautsMsgTemplateEntity();
        msgTemplateEntity.setFirstData("请注意，你的设备已远程开启成功", "#173177");
        msgTemplateEntity.setKeyword1Data("车载净化器", "#173177");
        msgTemplateEntity.setKeyword2Data(UID, "#173177");
        msgTemplateEntity.setKeyword3Data("在线", "#173177");
        String date=(new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        msgTemplateEntity.setKeyword4Data(date, "#173177");
        msgTemplateEntity.setKeyword5Data("启动中", "#173177");
        msgTemplateEntity.setRemarkData("点击详情查看车内空气质量哦！", "#173177");

        Map params = new HashMap();
        params.put("touser",openid);
        params.put("template_id",servletContext.getAttribute("template_id"));
        params.put("url","http://air.semsplus.com/rest/wx/monitoring?uid="+UID);


        Map<String,String> getParams = new HashMap<String,String>();
        getParams.put("access_token",accessToken.getAccess_token());
        params.put("data",msgTemplateEntity);
        String postData = StrUtils.MapToStr(params);

        try {
            WxRespCodeEntity wxRespCodeEntity = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST,getParams, new StringEntity(postData,"UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果："+ wxRespCodeEntity.getErrcode());
        }catch (Exception e){
            logger.error("",e);
        }
        return true;
    }

    public static boolean sendLoginOutMsg(String UID,ServletContext servletContext,String openid){
        AccessTokenEntity accessToken = (AccessTokenEntity) servletContext.getAttribute("accessToken");
        StautsMsgTemplateEntity msgTemplateEntity = new StautsMsgTemplateEntity();
        msgTemplateEntity.setFirstData("请注意，你的设备已远程断开连接", "#173177");
        msgTemplateEntity.setKeyword1Data("车载净化器", "#173177");
        msgTemplateEntity.setKeyword2Data(UID, "#173177");
        msgTemplateEntity.setKeyword3Data("不在线", "#173177");
        String date = (new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        msgTemplateEntity.setKeyword4Data(date, "#173177");
        msgTemplateEntity.setKeyword5Data("断线中", "#173177");
        msgTemplateEntity.setRemarkData("点击详情查看车内空气质量哦！", "#173177");

        Map params = new HashMap();
        params.put("touser", openid);
        params.put("template_id", servletContext.getAttribute("template_id"));
        params.put("url", "http://air.semsplus.com/rest/wx/go");


        Map<String, String> getParams = new HashMap<String, String>();
        getParams.put("access_token", accessToken.getAccess_token());

        params.put("data", msgTemplateEntity);
        String postData = StrUtils.MapToStr(params);

        try {
            WxRespCodeEntity wxRespCodeEntity  = WxUtil.sendRequest(WxUrlType.msgTemplateUrl, HttpMethod.POST, getParams, new StringEntity(postData, "UTF-8"), WxRespCodeEntity.class);
            logger.info("发送模版信息结果：" + wxRespCodeEntity.getErrcode());
        } catch (Exception e) {
            logger.error("", e);
        }
        return true;
    }
}
