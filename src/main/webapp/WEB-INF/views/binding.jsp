<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>设备绑定</title>
    <base href="<%=basePath%>" >
    <script type="text/javascript" src="js/lib/flexible.js"></script>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="css/sm.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_30orapuilrgm0a4i.css">
</head>

<body>
<div class="page-group">
    <!-- 单个page ,第一个.page默认被展示-->
    <div class="page">
        <!-- 这里是页面内容区 -->
        <div class="device-binding-content">
            <form id="add-form">
            <div class="binding-list-block list-block">

                <ul>
                    <!-- Text inputs -->
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-name"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">设备名称</div>
                                <div class="item-input">
                                    <input type="text" placeholder="设备名称" name="device_name">
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-email"></i></div>
                            <div class="item-inner">
                                <div class="item-title label">设备UID</div>
                                <div class="item-input">
                                    <input type="text" placeholder="设备UID" name="device_uid">
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="binding-content-block content-block">
                <div class="row">
                    <div class="col-50"><a href="#device-list" class="button  button-fill button-danger">取消</a></div>
                    <div class="col-50"><a href="javascript:add()" class="button  button-fill  button-success">提交</a></div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<script type='text/javascript' src='js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='js/lib/sm.js' charset='utf-8'></script>
<script type="text/javascript" src="js/binding.js"></script>

</body>

</html>
</html>