<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>设备管理</title>
    <base href="<%=basePath%>">
    <script type="text/javascript" src="js/lib/flexible.js"></script>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="css/sm.css">
    <link rel="stylesheet" type="text/css" href="css/layout.css">
</head>

<body>
<div class="page-group">
    <!-- 单个page ,第一个.page默认被展示-->
    <div class="page">
        <!-- 这里是页面内容区 -->
        <div class="binding-content" style="display: none;">
            <!-- 这里是未绑定状态 -->
            <div class="binding-state">
                <span class="icon iconfont icon-xiaolian"></span>
            </div>
            <div class="binding-p">
                您还没有绑定设备，请先进行绑定
            </div>
            <!-- 这里是设备绑定的按钮 -->
            <div class="binding-button">
                <a href="rest/wx/binding" class="button button-success">设备绑定</a>
            </div>
        </div>

        <div class="monitoring-content" style="display: none;">
            <div class="card monitoring-card">
                <div class="card-content">
                    <div class="list-block">
                        <ul id="devices">

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='js/lib/sm.js' charset='utf-8'></script>
<script type="text/javascript" src="js/index.js?170610001"></script>
</body>

</html>


