<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    long nowTime = new Date().getTime();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>${name}</title>
    <base href="<%=basePath%>">
    <script type="text/javascript" src="js/lib/flexible.js"></script>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="css/sm.css">
    <link rel="stylesheet" type="text/css" href="css/module.css">
</head>
<body>
<div class="page-group">
    <!-- 单个page ,第一个.page默认被展示-->
    <div class="page">
        <!-- 这里是页面内容区 -->
        <div class="content time-content">
            <input type="hidden" id="uid" value="${e.device_uid}">
            <input type="hidden" id="taskTime" value="${taskTime}">
            <div class="content-block-title">任务I</div>
            <div class="card">
                <!-- 这里是信息设置关于开机内容 -->
                <div class="card-content">
                    <div class="list-block">
                        <div class="item-content">
                            <div class="item-media"><i class="icon icon-form-toggle"></i></div>
                            <div class="item-inner">
                                <div class="item-title-row">
                                    <input class="item-title picker-time" value="11 : 15" id="timeA"/>
                                    <span class="item-text repeat-input" id="infoA" data-time="1111111" >每天</span>
                                </div>
                                <label class="label-switch">
                                    <input type="checkbox" id="statusA">
                                    <div class="checkbox"></div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-content">
                    <div class="list-block">
                        <ul>
                            <li>
                                <a href="javascript:;" class="item-link item-content repeat-select" task-index="A">
                                    <div class="item-media"><i class="icon icon-f7"></i></div>
                                    <div class="item-inner">
                                        <div class="item-title">重复</div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- 这里是信息设置关于关机内容 -->
            <div class="content-block-title">任务II</div>
            <div class="card">
                <div class="card-content">
                    <div class="list-block">
                        <ul>
                            <li>
                                <div class="item-content">
                                    <div class="item-media"><i class="icon icon-form-toggle"></i></div>
                                    <div class="item-inner">
                                        <div class="item-title-row">
                                            <input class="item-title picker-time" value="17 : 15" id="timeB"/>
                                            <span class="item-text repeat-input" id="infoB" data-time="1111111">每天</span>
                                        </div>
                                    </div>
                                    <label class="label-switch">
                                        <input type="checkbox" id="statusB">
                                        <div class="checkbox"></div>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-content">
                    <div class="list-block">
                        <ul>
                            <li>
                                <a href="javascript:;" class="item-link item-content repeat-select" task-index="B">
                                    <div class="item-media"><i class="icon icon-f7"></i></div>
                                    <div class="item-inner">
                                        <div class="item-title">重复</div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- 这里是保存按钮 -->
            <div class="save-button">
                <a href="javascript:;" class="button button-success">保存</a>
            </div>
        </div>
    </div>
</div>
<div class="popup repeatPopup">
    <div class="list-block media-list">
        <ul>
            <li>
                <button class="close-popup cancelBtn">取消</button>
                <button class="close-popup ensureBtn">确定</button>
            </li>
            <li><label class="label-checkbox item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">星期日</div>
                    </div>
                </div>
                <input type="checkbox" name="my-radio" checked>

                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
            </label></li>
            <li><label class="label-checkbox item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">星期六</div>
                    </div>
                </div>
                <input type="checkbox" name="my-radio" checked>

                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
            </label></li>
            <li><label class="label-checkbox item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">星期五</div>
                    </div>
                </div>
                <input type="checkbox" name="my-radio" checked>

                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
            </label></li>
            <li><label class="label-checkbox item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">星期四</div>
                    </div>
                </div>
                <input type="checkbox" name="my-radio" checked>

                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
            </label></li>
            <li><label class="label-checkbox item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">星期三</div>
                    </div>
                </div>
                <input type="checkbox" name="my-radio" checked>

                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
            </label></li>
            <li><label class="label-checkbox item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">星期二</div>
                    </div>
                </div>
                <input type="checkbox" name="my-radio" checked>

                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
            </label></li>
            <li><label class="label-checkbox item-content">
                <div class="item-inner">
                    <div class="item-title-row">
                        <div class="item-title">星期一</div>
                    </div>
                </div>
                <input type="checkbox" name="my-radio" checked>
                <div class="item-media"><i class="icon icon-form-checkbox"></i></div>
            </label></li>
        </ul>
    </div>
</div>
<script type='text/javascript' src='js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='js/lib/sm.js' charset='utf-8'></script>
<script type='text/javascript' src='js/time.js?<%=nowTime%>' charset='utf-8'></script>
</body>
</html>
