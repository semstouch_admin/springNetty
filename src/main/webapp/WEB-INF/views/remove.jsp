<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>解绑设备</title>
    <base href="<%=basePath%>">
    <script type="text/javascript" src="js/lib/flexible.js"></script>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link rel="stylesheet" href="css/sm.css">
    <link rel="stylesheet" type="text/css" href="css/master.css">
    <link rel="stylesheet" type="text/css" href="http://at.alicdn.com/t/font_30orapuilrgm0a4i.css">
</head>
<body>
<div class="page-group">
    <!-- 单个page ,第一个.page默认被展示-->
    <div class="page">
        <div class="content remove-content">
            <div class="card remove-card">
                <!-- 这里是设备的相关信息内容 -->
                <div class="card-content">
                    <div class="list-block">
                        <ul>
                            <li>
                                <i class="icon icon-f7"></i>
                                <div class="item-inner">
                                    <div class="item-title">设备名称</div>
                                    <div class="item-after one-item-after">${e.device_name}</div>
                                </div>
                                </a>
                            </li>
                            <li>
                                <i class="icon icon-f7"></i>
                                <div class="item-inner">
                                    <div class="item-title">设备UID</div>
                                    <div class="item-after one-item-after">${e.device_uid}</div>
                                </div>
                                </a>
                            </li>
                            <li>
                                <i class="icon icon-f7"></i>
                                <div class="item-inner">
                                    <div class="item-title">设备版本</div>
                                    <div class="item-after">V1.0</div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- 解绑设备的按钮 -->
            <div class="remove-button">
                <a  id="removeBtn" href="javascript:;" class="button  button-fill button-danger">解绑设备</a>
            </div>
        </div>
    </div>
</div>
<script type='text/javascript' src='js/lib/zepto.js' charset='utf-8'></script>
<script type='text/javascript' src='js/lib/sm.js' charset='utf-8'></script>
<script type="text/javascript" src="js/remove.js"></script>
</body>

</html>