var uid;
var socket;
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};
/**
 * 指令解析
 * @param data
 */
function cmdSet(data){
    if(data.cmd=="webLoginActor"){
        if(data.data=="active"){
            sendWS("sendDataActor",uid,"28","01");
            $(".auto-title").attr("status","active");
            $(".state-card").css("background-color", "#1ba365");
        }else{
            $(".auto-title").text("不在线");
            $(".auto-title").attr("status","noActive");
            $(".state-card").css("background-color", "#999999");
        }
    }
    if(data.cmd=="F1"){
        var rsData= data.data;

        $("#filterScreenBtn").attr("status","00");
        $("#clockBtn").attr("status","00");
        var temperature=rsData.substring(4,6)+rsData.substring(2,4);
        $("#temperatureInfo").html(parseInt(temperature,16)/10);
        $("#humidityInfo").html(parseInt(rsData.substring(8,10)+rsData.substring(6,8),16)/10);
        $("#pmInfo").html(parseInt(rsData.substring(12,14)+rsData.substring(10,12),16));
        var vocInfo =rsData.substring(14,16);
        if(vocInfo=="00"){
            $(".excellent-title").html("优");
        }
        if(vocInfo=="01"){
            $(".excellent-title").html("良");
        }
        if(vocInfo=="02"){
            $(".excellent-title").html("中");
        }
        if(vocInfo=="03"){
            $(".excellent-title").html("差");
        }
        $(".auto-title").html(rsData.substring(18,20)=="01"?"自动运行":"手动运行");

        $("#autoBtn").attr("status",rsData.substring(18,20));
        //判断是否是否全开
        $("#windSpeedBtn").attr("status",rsData.substring(22,24));
        $("#plasmaBtn").attr("status",rsData.substring(24,26));
        $("#lightBtn").attr("status",rsData.substring(26,28));
        $("#indicateBtn").attr("status",rsData.substring(28,30));

        if(rsData.substring(24,26)=="01"||rsData.substring(26,28)=="01"||rsData.substring(28,30)=="01"||rsData.substring(22,24)!="00"){
            $("#switchBtn").attr("status","01");
        }else{
            $("#switchBtn").attr("status","00");
        }
        initBtn();


    }
    if(data.cmd=="80"){

    }
    if(data.cmd=="01"){
        var rsData= data.data;
        if(rsData.substring(0,2)=="28"){
            $("#filterScreenBtn").attr("status","00");
            $("#clockBtn").attr("status","00");
            var temperature=rsData.substring(4,6)+rsData.substring(2,4);
            $("#temperatureInfo").html(parseInt(temperature,16)/10);
            $("#humidityInfo").html(parseInt(rsData.substring(8,10)+rsData.substring(6,8),16)/10);
            $("#pmInfo").html(parseInt(rsData.substring(12,14)+rsData.substring(10,12),16));
            var vocInfo =rsData.substring(14,16);
            if(vocInfo=="00"){
                $(".excellent-title").html("优");
            }
            if(vocInfo=="01"){
                $(".excellent-title").html("良");
            }
            if(vocInfo=="02"){
                $(".excellent-title").html("中");
            }
            if(vocInfo=="03"){
                $(".excellent-title").html("差");
            }
            $(".auto-title").html(rsData.substring(18,20)=="01"?"自动运行":"手动运行");

            $("#autoBtn").attr("status",rsData.substring(18,20));
            //判断是否是否全开
            $("#windSpeedBtn").attr("status",rsData.substring(22,24));
            $("#plasmaBtn").attr("status",rsData.substring(24,26));
            $("#lightBtn").attr("status",rsData.substring(26,28));
            $("#indicateBtn").attr("status",rsData.substring(28,30));

            if(rsData.substring(24,26)=="01"||rsData.substring(26,28)=="01"||rsData.substring(28,30)=="01"||rsData.substring(22,24)!="00"){
                $("#switchBtn").attr("status","01");
            }else{
                $("#switchBtn").attr("status","00");
            }
            initBtn();
        }
    }

}

/**
 * 初始化按钮样式
 */
function initBtn(){
   $(".monitoring-button div span").each(function(){
       if($(this).attr("status")=="00"){
           $(this).css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
       }else{
           $(this).css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
       }
    });
}

/**
 * 初始化页面
 */
function initWebSocket(){
    if (!window.WebSocket) {
        window.WebSocket = window.MozWebSocket;
    }
    if (window.WebSocket) {
        $.toast("连接中...");
        socket = new WebSocket("ws://"+window.location.hostname+":5888/ws");
        socket.onmessage = function(event) {
            var data =JSON.parse(event.data);
            cmdSet(data)
        };
        socket.onopen = function(event) {
           uid=GetQueryString("uid");
            sendWS("webLoginActor",uid,"","");
        };
        socket.onclose = function(event) {
            initWebSocket();
        };
        socket.onerror=function(event){
            initWebSocket();
            $.alert('网络异常!');
        };
    } else {
        $.alert('您的手机暂不支持!');
    }
}
/**
 * 发送指令方法
 * @param cmd
 * @param uid
 * @param data
 * @param code
 */
function sendWS(cmd,uid,data,code){
    if (!window.WebSocket) {
        return;
    }
    if (socket.readyState == WebSocket.OPEN) {
        var msgJson = { "cmd": cmd, "uid":uid,"data": data ,"code":code};
        socket.send(JSON.stringify(msgJson));
    } else {
        $.toast("网络不给力...");
    }
}

/**
 * URL上获取参数
 * @param name
 * @returns {null}
 * @constructor
 */
function GetQueryString(name) {
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)return  unescape(r[2]); return null;
}


initWebSocket();
/*监控页面按钮部分*/
//开关按钮
$("#switchBtn").click(function() {
    if( $(".auto-title").attr("status")=="noActive"){
        return;
    }
    if ($(this).attr("status") == "01") {
        var  data= "0000";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","00");
        $(this).css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
    } else {
        var  data= "0001";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","01");
        $(this).css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
    }
});
//AUTO按钮
$("#autoBtn").click(function() {
    if( $(".auto-title").attr("status")=="noActive"){
        return;
    }
     if ($(this).attr("status") == "01") {
        var  data= "00";
        sendWS("sendDataActor",uid,data,"90");
        $(this).attr("status","00");
         $(this).css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
         $(".auto-title").html("手动模式");
     } else {
        var  data= "01";
        sendWS("sendDataActor",uid,data,"90");
        $(this).attr("status","01");
         $(this).css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" });
         $(".auto-title").html("自动模式");
     }
});
//氛围灯
$("#lampBtn").click(function() {
    if( $(".auto-title").attr("status")=="noActive"){
        return;
    }
     if ($(this).attr("status") == "01") {
        var  data= "0600";
        sendWS("sendDataActor",uid,data,"02");
        $(this).attr("status","00");
         $(this).css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
     } else {
        var  data= "0500";
        sendWS("sendDataActor",uid,data,"02");
        $(this).attr("status","01");
         $(this).css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
     }
});
//风速按钮
$("#windSpeedBtn").click(function() {
    var buttons1 = [
{
        text: 'IV档',
        onClick: function() {
            if( $(".auto-title").attr("status")=="noActive"){
                return;
            }
            var  data= "016400";
            sendWS("sendDataActor",uid,data,"f5");
            $("#windSpeedBtn").css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
        }
    },{
        text: 'III档',
        onClick: function() {
            if( $(".auto-title").attr("status")=="noActive"){
                return;
            }
            var  data= "014B00";
            sendWS("sendDataActor",uid,data,"f5");
            $("#windSpeedBtn").css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
        }
    }, {
        text: 'II档',
        onClick: function() {
            if( $(".auto-title").attr("status")=="noActive"){
                return;
            }
            var  data= "013200";
            sendWS("sendDataActor",uid,data,"f5");
            $("#windSpeedBtn").css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
        }
    }, {
        text: 'I档',
        onClick: function() {
            if( $(".auto-title").attr("status")=="noActive"){
                return;
            }
            var  data= "011700";
            sendWS("sendDataActor",uid,data,"f5");
            $("#windSpeedBtn").css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
        }
    }];
    var buttons2 = [{
        text: '关闭',
        onClick: function() {
            if( $(".auto-title").attr("status")=="noActive"){
                return;
            }
            var  data= "010300";
            sendWS("sendDataActor",uid,data,"f5");
            $("#windSpeedBtn").css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
        }
    }];
    var groups = [buttons1, buttons2];
    $.actions(groups);
});

//负离子
$("#plasmaBtn").click(function() {
    if( $(".auto-title").attr("status")=="noActive"){
        return;
    }
    if ($(this).attr("status") == "01") {
        var  data= "04";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","00")
        $(this).css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
    } else {
        var  data= "030300";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","01")
        $(this).css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
    }
});

//氛围灯
$("#lightBtn").click(function() {
    if( $(".auto-title").attr("status")=="noActive"){
        return;
    }
    if ($(this).attr("status") == "01") {
        var  data= "0600";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","00")
        $(this).css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
    } else {
        var  data= "0500";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","01")
        $(this).css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
    }
});


//指示灯
$("#indicateBtn").click(function() {
    if( $(".auto-title").attr("status")=="noActive"){
        return;
    }
    if ($(this).attr("status") == "01") {
        var  data= "0601";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","00")
        $(this).css({ "background-color": "#fff", "color": "#3d4145", "border": "0.05rem #808080 solid" });
    } else {
        var  data= "0501";
        sendWS("sendDataActor",uid,data,"f5");
        $(this).attr("status","01")
        $(this).css({ "background-color": "#1aa365", "color": "#fff", "border": "0.05rem #1aa365 solid" })
    }
});



$("#clockBtn").click(function() {
    window.location.href="http://air.semsplus.com/rest/wx/setTime?uid="+uid;
});

//重置滤网
$("#filterScreenBtn").click(function() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: "rest/device/restFilter",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        data: {uid:uid},
        success: function(data) {
            if(data.message=="success") {
                $.alert('重置成功！');
                $("#filterInfo").html("1000");
            }
            if(data.message=="noIn") {
                $.alert('设备不存在！');
            }
        }
    })
});

//点击更多
$("#moreBtn").click(function(){
    window.location.href="http://air.semsplus.com/rest/wx/remove?uid="+uid;
});
$.config={
    router:false
}

$.init();