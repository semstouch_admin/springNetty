/**
 * 查询全部设备
 */
function query() {
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/rest/device/find",
        data: { _method: "GET" },
        success: function(data) {
            var typeList = data.data.list;
            var listHtml = "";
            if(typeList.length==0){
                $('.binding-content').show();
            }else {
                $.each(typeList, function (i, n) {
                    listHtml += "<li>" +
                        "<a data-id='"+ n.device_uid+"' class='item-link item-content d-detail' href='javascript:;'>" +
                        "<div class='item-media'><i class='icon icon-f7'></i></div>" +
                        "<div class='item-inner'>" +
                        "<div class='item-title'>" +n.device_name + "</div>" +
                        "</div> </a> </li>"
                })
                $("#devices").html(listHtml);
                $('.monitoring-content').show();
                $(".d-detail").click(function () {
                    var uid = $(this).attr("data-id");
                    window.location.href = "rest/wx/monitoring?uid=" + uid;
                });
            }
        }
    });
}

query();

$.config={
    router:false
}

$.init();
