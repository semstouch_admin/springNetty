var taskIndex;

 //重复按钮弹出星期框
 $(".repeat-select").click(function() {
     taskIndex=$(this).attr("task-index");
     $.popup(".repeatPopup");
 });
 $(".ensureBtn").click(function(){
     var result = "";
     $("input[name='my-radio']").each(function() {
         if($(this).attr("checked")==true){
             result += '1';
         }else{
             result += '0';
         }
     });
     $("#info"+taskIndex).attr("data-time",result);
     $("#info"+taskIndex).html(getWeekDay(result));
 });

 function getWeekDay(val){
     var weekDay=["星期日","星期六","星期五","星期四","星期三","星期二","星期一"];
     var result="";
     var valArr=val.replace(/(.)(?=[^$])/g,"$1,").split(",");
     if(val=="1111111"){
         return "每天";
     }
     $.each(valArr,function(index,value){
         if(value=='1'){
             result+=weekDay[index]+","
         }
     });
     return result;
 }
   $(".picker-time").picker({
         cols: [{
             textAlign: 'center',
             values: ['00','01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17','18', '19', '20', '21', '22', '23']
         }, 
          {
             textAlign: 'center',
             values: [':']
         },
         {
             textAlign: 'center',
             values: ['00','01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59']
         }],
         toolbarTemplate: '<header class="bar bar-nav">\
  <button class="button button-link pull-right close-picker">确定</button>\
  </header>'
     });


function getHxData(data){
    return data.length==1?("0"+data):data;
}


 $(".button-success").click(function(){
     var taskTime="";

     if($("#statusA").attr("checked")==true){
         taskTime+="00";
         taskTime+=getHxData(parseInt($("#infoA").attr("data-time"),2).toString(16));
         var timeArr=$("#timeA").val().split(":");
         taskTime+=getHxData(parseInt(timeArr[0],10).toString(16));
         taskTime+=getHxData(parseInt(timeArr[1],10).toString(16));
         taskTime+="0f00";
     }
     if($("#statusB").attr("checked")==true){
         taskTime+="01";
         taskTime+=getHxData(parseInt($("#infoB").attr("data-time"),2).toString(16));
         var timeArr=$("#timeB").val().split(":");
         taskTime+=getHxData(parseInt(timeArr[0],10).toString(16));
         taskTime+=getHxData(parseInt(timeArr[1],10).toString(16));
         taskTime+="0f00";
     }

     if(taskTime==""){
         $.alert('未设置任务！');
     }else{
         $.ajax({
             type: "POST",
             dataType: "json",
             url: "rest/device/task",
             headers: {
                 "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"
             },
             data:{
                 uid:$("#uid").val(),
                 taskTime:taskTime
             },
             success: function(data) {
                 if(data.message=="success") {
                     $.alert('设置成功！');
                 }
                 if(data.message=="isnO") {
                     $.alert('设备不存在！');
                 }
                 if(data.message=="onLogin") {
                     window.location.href = "rest/wx/go";
                 }
             }
         })
     }


 })


$.init();

